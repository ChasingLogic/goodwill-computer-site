package main

import(
	"fmt"
	"net/http"
	"html/template"
)

func rootHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Connection Recieved")
	tmplt, lerr := template.ParseFiles("html/index.html")
	if lerr != nil {
		http.Error(w, lerr.Error(), http.StatusInternalServerError)
	}
	err := tmplt.Execute(w, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


func main() {
	http.HandleFunc("/", rootHandler)
	http.Handle("/resources/", http.StripPrefix("/resources/", http.FileServer(http.Dir("html/resources"))))
	http.ListenAndServe(":8080", nil)
}
