var current = $("#One");	
var old = $();
var slideshow = null;
var delay = 10000;
//var activity = false;


jQuery.fn.navHighlight = function (target) {
	if(typeof(target) == 'undefined') target = "One";
	old = current;
	current = $("#" + target);
	var distance = current.offset().left - old.offset().left;
	movement = {"left": "+=" + distance};
	$("#highlight").animate(movement, 300, "linear");
};

jQuery.fn.manualMove = function(target) {
	var slideToLoad = target.attr("id");
	if(typeof(slideToLoad) == 'undefined') slideToLoad = 'One';
	$("#content").load("/resources/content.html #content" + slideToLoad , function() {
		jQuery.fn.navHighlight(slideToLoad);
		$('.textbox').hide().fadeIn('slow');
	});
}

jQuery.fn.autoMove = function(target) {
	var slideToLoad = target.attr("id");
	if(typeof(slideToLoad) == 'undefined') slideToLoad = 'One';
	$("#content").load("/resources/content.html #content" + slideToLoad , function() {
		jQuery.fn.navHighlight(slideToLoad);
		$('.textbox').hide().fadeIn('slow');
	});
	if(delay != 10000){
		delay = 10000;
		clearInterval(slideshow);
		slideshow = setInterval(function() { jQuery.fn.manualMove(current.next()) }, delay);
	}
}

$("document").ready(function(){

	current = $("#One");

	slideshow = setInterval(function() { jQuery.fn.autoMove(current.next()) }, delay);
	
	$(document).keydown(function(e) {
    	if(e.keyCode == 39){
			jQuery.fn.manualMove(current.next());
		}
		else if(e.keyCode == 37 && current.attr("id") == "One"){
			jQuery.fn.manualMove($("#Seven"));
		}
		else if(e.keyCode == 37){
			jQuery.fn.manualMove(current.prev());
		}
	});
	
	$(".button").click(function() {
		delay = 20000;
		clearInterval(slideshow);
		slideshow = setInterval(function() { jQuery.fn.autoMove(current.next()) }, delay);
	})

	$("#content").load("/resources/content.html #contentOne", function() {
		$(".textbox").hide().fadeIn('slow');
	});
	$("#One").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Two").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Three").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Four").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Five").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Six").click(function(){
		jQuery.fn.manualMove($(this));
	});
	$("#Seven").click(function(){
		jQuery.fn.manualMove($(this));
	});

});
